'use strict';
const express = require("express");
const promises = require("bluebird");
const path = require("path");
var ScrapeModels = require('../Models/ScrapeImages');
const fs = require("fs"),
  gm = require("gm");
const mongoose = require("mongoose");

const numImagesToScrap = 5;
const imageDir = path.join(__dirname, "../public/images/");

const request = require("request");


exports.saveImages = function(request,response) {
  if (request && request.body.text){
      let criteria={
          text: request.body.text,
          createdAt: { $gte: new Date() - 24 * 60 * 60 * 1000 }
      };
      let projection={
        _id:1
      };
      let options={
        lean:true
      };
      ScrapeModels
          .find(criteria,projection,options)
          .then(result => {
          if (result.length > 0) {
              let err='Already scraped within 24 hours';
              res.send(err);
          } else {
              return imageScrapingFromGoogle(request.body.text)
                  .then(result => {
                      response.send("Image saved in public/images folder " );
                  })
                  .catch(err => {
                      console.log("hhhhhhhhhhhh",err)
                      response.send("error:" + err);
                  });
          }
      });

  }else {
      response.send('Please enter text for save image');
  }

}

exports.getImagesById = function(request,response) {
    console.log("reqeuuuuuuuu",request.query,request.headers.host)
    if (!mongoose.Types.ObjectId.isValid(request.query.id)) {
        response.send("Invalid object Id");
    }else {
        ScrapeModels
            .findOne({
                _id: mongoose.Types.ObjectId(request.query.id)
            })
            .then(result => {
                console.log("result*******",result)
                if (result) {
                    result.images = result.images.map(item => {
                        return "http://" + request.headers.host + "/public/images/" + item;
                    });
                    response.send(result);
                  //  response.render("../views/images.ejs", { result: result });
                } else {
                    response.send("error: Can not find object");
                }
            });
    }
}

exports.getAllSearchImages = function(request,response) {
    console.log("reqeuuuuuuuu",request.query,request.headers.host)
    ScrapeModels
        .find(
            {},
            {
                text: 1
            }
        )
        .then(result => {
            console.log(result);
            if (result.length > 0) {
                response.render("../public/views/keywords.ejs", { result: result });
            } else {
                response.send("error: no searches yet");
            }
        });
}

function imageScrapingFromGoogle(text) {
  let Scraper = require("images-scraper"),
    google = new Scraper.Google();
  console.log('text to scrape',text);

  return google
    .list({
      keyword: text,
      num: numImagesToScrap,
      detail: true,
      advanced: {
                imgType: 'jpg', // options: clipart, face, lineart, news, photo
             //   resolution: undefined, // options: l(arge), m(edium), i(cons), etc.
            //    color: undefined // options: color, gray, trans
            }
    })
    .then(function(result) {
        let i=0
      const promisifiedUrl = promises.promisify(resizeImages);
      return promises
        .map(result, (obj) => {
            let imageFilesExt = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG'];
            let ext = obj.url.substr(obj.url.lastIndexOf('.'));
            console.log("keyword******************",ext)
            if (imageFilesExt.indexOf(ext) >= 0) {
                const key = obj.url
                    .split("/")
                    .slice(-1)
                    .pop()
                    .split("?")[0];
                //   const key = keyword + index + new Date();
             //   console.log("keyword******************",key)
                return promisifiedUrl(obj.url, "scrape", key).then(
                    res => {
                        return promises.resolve(key);
                    }
                );
            }

        })
        .then(res => {
          return promises.resolve(res);
        });
    })
    .then(res => {
      const scrapObj = new ScrapeModels();
      scrapObj.text = text;
      scrapObj.images = res;
      scrapObj.requestedAt = new Date();
      console.log("objtoSave******************",scrapObj)
      scrapObj.save().then(res => {
        console.log(res);
        return promises.resolve(res);
      });
    })
    .catch(function(err) {
      return promises.reject(err);
    });
}

function resizeImages(url, bucket, key, callback) {
    console.log("resizeImages****************",url)
  gm(request(url))
    .resize("360", "360")
    .monochrome()
    .write(`${imageDir}${key}`, callback);
}
