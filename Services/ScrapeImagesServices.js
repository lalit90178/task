'use strict';

var ScarapeImages = require('../Models/ScrapeImages');

//Get ScarapeImages from DB
var getScarapeImages = function (criteria, projection, options, callback) {
    ScarapeImages.find(criteria, projection, options, callback);
};

//Insert ScarapeImages in DB
var createScarapeImages = function (objToSave, callback) {
    new ScarapeImages(objToSave).save(callback)
};

//Update ScarapeImages in DB
var updateScarapeImages = function (criteria, dataToSet, options, callback) {
    ScarapeImages.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var removeScarapeImages = function (criteria, callback) {
    ScarapeImages.findOneAndRemove(criteria, callback);
};


module.exports = {
    getScarapeImages: getScarapeImages,
    createScarapeImages: createScarapeImages,
    updateScarapeImages: updateScarapeImages,
    removeScarapeImages: removeScarapeImages,
};

