var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ScarapeImages = new Schema({
    text:{type: String},
    images: [{ type: String }],
    createdAt: { type: Date,default: Date.now() }
});

var ScarapeImages = mongoose.model("ScarapeImages", ScarapeImages);
module.exports = ScarapeImages;