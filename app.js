'use strict';
const express = require('express');
const path = require('path');

const request = require('request');
const http= require('http');
var cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const mongoose = require("mongoose")
const scrape = require("./routes/scrape");

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


mongoose.connect("mongodb://localhost/task", { useMongoClient: true });
const port = 3000;
app.set('view engine', 'jade');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({ limit: "50mb",extended: false}));
app.use(cors());
app.get('/', function (req, res) {

    console.log(".........................................................................................");
    res.render('main');
});

app.get("/public/images/:filename", function(req, res) {
    res.sendfile("./public/images/" + req.params.filename);
});
app.post('/saveImages',scrape.saveImages);
app.get('/getImagesById',scrape.getImagesById);
app.get('/getAllSearchImages',scrape.getAllSearchImages);




const server = http.createServer(app).listen(port, function () {
    console.log("Express server listening on port " + port);
});



