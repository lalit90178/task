'use strict';

var Models = require("../models/Scraoe");

//Get Post from DB
var getPost = function (criteria, projection, options, callback) {
    Models.Post.find(criteria, projection, options, callback);
};

//Insert User in DB
var createPost = function (objToSave, callback) {
    new Models.Post(objToSave).save(callback)
};

//Update User in DB
var updatePost = function (criteria, dataToSet, options, callback) {
    Models.Post.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var removePost = function (criteria, callback) {
    Models.Post.findOneAndRemove(criteria, callback);
};


var getPostPopulate = function (criteria, projection, options, populateArray, callback) {
    Models.Post.find(criteria, projection, options).populate(populateArray).exec(function (err, result) {
        callback(null, result)
    });
};

var getPostAggregate = function (criteria, dataToSet, options, callback) {
    Models.Post.aggregate(
        [
            criteria,
            dataToSet,
            options
        ])
        .exec(function (error, result) {
            //console.log(result)
            callback(error, result);
        });
}

module.exports = {
    getPost: getPost,
    createPost: createPost,
    updatePost: updatePost,
    removePost: removePost,
    getPostAggregate: getPostAggregate,
    getPostPopulate:getPostPopulate
};

